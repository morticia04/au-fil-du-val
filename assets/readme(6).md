# Ecommerce Symfony
- [Ecommerce Symfony](#ecommerce-symfony)
  - [1. Vérifiez les pré-requis (php \> 8, mysql, composer, symfony.exe)](#1-vérifiez-les-pré-requis-php--8-mysql-composer-symfonyexe)
    - [1.1 Version PHP](#11-version-php)
    - [1.2 Version Myqsl](#12-version-myqsl)
    - [1.3 Version Composer](#13-version-composer)
    - [1.4 Version symfony](#14-version-symfony)
  - [2. Installation de Symfony (regarder la doc)](#2-installation-de-symfony-regarder-la-doc)
    - [2.1. Installation](#21-installation)
    - [2.2. Configuration du .git](#22-configuration-du-git)
  - [3. Création de la configuration locals (.env.local) : et création database](#3-création-de-la-configuration-locals-envlocal--et-création-database)
  - [4. Démarrage server Symfony :](#4-démarrage-server-symfony-)
  - [5. Création du premier controller (page d'accueil /)](#5-création-du-premier-controller-page-daccueil-)
  - [6. Création de nos entités à partir du MCD ou du diagramme de classe](#6-création-de-nos-entités-à-partir-du-mcd-ou-du-diagramme-de-classe)
    - [6.1 Création de la base de données](#61-création-de-la-base-de-données)
    - [6.2 Création des entités sauf user](#62-création-des-entités-sauf-user)
    - [6.3 Création de l'entité user](#63-création-de-lentité-user)
  - [7. Relation entre nos entités](#7-relation-entre-nos-entités)
  - [8. Migration vers la base de données ou Mise à jour du schéma](#8-migration-vers-la-base-de-données-ou-mise-à-jour-du-schéma)
  - [9. Création de nos controllers admin (prefixer avec Admin : exemple : AdminProductController) pour créer de la donnée (CRUD). ¨Product - Category - Tva](#9-création-de-nos-controllers-admin-prefixer-avec-admin--exemple--adminproductcontroller-pour-créer-de-la-donnée-crud-product---category---tva)
  - [10.](#10)
  - [11. Eventuellement faire des Fixtures (création de jeux de données fake ou pas)](#11-eventuellement-faire-des-fixtures-création-de-jeux-de-données-fake-ou-pas)
    - [11.1. Installation de DoctrineFixtureBundle](#111-installation-de-doctrinefixturebundle)
    - [11.2. Vérifier la configuration.](#112-vérifier-la-configuration)
    - [11.3. Installation de Faker](#113-installation-de-faker)
    - [11.4. Création de données Fake](#114-création-de-données-fake)
    - [11.5. Injection dans la base de données](#115-injection-dans-la-base-de-données)
  - [12. Petites choses en plus :](#12-petites-choses-en-plus-)
    - [12.1.  Modifier le Type (Form) : définir le type des champs nécessaire](#121--modifier-le-type-form--définir-le-type-des-champs-nécessaire)
    - [12.2.  Gérer le slug](#122--gérer-le-slug)
    - [12.3.  Créer le layout de l'admin et l'associer aux vues admin](#123--créer-le-layout-de-ladmin-et-lassocier-aux-vues-admin)
    - [12.4.  Ajouter boostrap à l'admin et form twig avec bootstrap](#124--ajouter-boostrap-à-ladmin-et-form-twig-avec-bootstrap)
    - [12.5.  Ajouter une navigation à l'admin](#125--ajouter-une-navigation-à-ladmin)
    - [12.6.  Optimiser l'ajout et l'édit dans les controller CRUD](#126--optimiser-lajout-et-lédit-dans-les-controller-crud)
    - [12.7. Installation du webpack encore](#127-installation-du-webpack-encore)
      - [12.7.1. Installation du bundle](#1271-installation-du-bundle)
      - [12.7.2. Installation de la dépendence npm](#1272-installation-de-la-dépendence-npm)
      - [12.7.3. mettre à jour le dossier asset de public](#1273-mettre-à-jour-le-dossier-asset-de-public)
    - [12.8. Point d'entrée pour les différentes mise en forme](#128-point-dentrée-pour-les-différentes-mise-en-forme)
  - [13. Upload des images](#13-upload-des-images)
    - [13.1. Modifier le fichier Form-\>CategoryType.php pour avoir un champ de type File](#131-modifier-le-fichier-form-categorytypephp-pour-avoir-un-champ-de-type-file)
    - [13.2. Modifier la sauvegarde de l'image](#132-modifier-la-sauvegarde-de-limage)
    - [13.3. Dans le controller tester la réception du fichier, testez l'upload directement à partir du controller](#133-dans-le-controller-tester-la-réception-du-fichier-testez-lupload-directement-à-partir-du-controller)
    - [13.4. Créer un service d'unpload... le targetDirectory doir être public/uploads/... la méthode upload du service doit recevoir un deuxième paramètre $subDirectory. Par exemple 'category'. Le fichier sera donc uploadé dans public\\uploads\\category...](#134-créer-un-service-dunpload-le-targetdirectory-doir-être-publicuploads-la-méthode-upload-du-service-doit-recevoir-un-deuxième-paramètre-subdirectory-par-exemple-category-le-fichier-sera-donc-uploadé-dans-publicuploadscategory)
  - [14. Afficher sur le Front nos produits (sur la page front d'accueil)](#14-afficher-sur-le-front-nos-produits-sur-la-page-front-daccueil)
  - [15. Page de détails produits](#15-page-de-détails-produits)
  - [16. Ajouter la gestion du multilangue (en|fr)](#16-ajouter-la-gestion-du-multilangue-enfr)
  - [17. Ajouter une méthode Front pour la gestion des pages statiques (presentation mentions légales, contacts...)](#17-ajouter-une-méthode-front-pour-la-gestion-des-pages-statiques-presentation-mentions-légales-contacts)
  - [18. Création du register client](#18-création-du-register-client)
  - [19. Création du login client](#19-création-du-login-client)
  - [20. Et après :](#20-et-après-)
    - [20.1.  Ajout au panier (session)](#201--ajout-au-panier-session)
    - [20.2.  Validation de la commande](#202--validation-de-la-commande)
    - [20.3.  Paiement !](#203--paiement-)


## 1. Vérifiez les pré-requis (php > 8, mysql, composer, symfony.exe)  
vérfier instalation de scoop et l'installer si besoin
	installer symfony : 
  ```bash
  scoop install symfony-cli
  ```
### 1.1 Version PHP  
``` bash
PS C:\wamp64\www\ADF22\adf-aurelia\symfony\testDecembre> php -v
PHP 8.1.0 (cli) (built: Nov 23 2021 21:48:28) (ZTS Visual C++ 2019 x64)
Copyright (c) The PHP Group
Zend Engine v4.1.0, Copyright (c) Zend Technologies
```
### 1.2 Version Myqsl  
Ouvrir WAMP  

### 1.3 Version Composer  
``` bash
PS C:\wamp64\www\ADF22\adf-aurelia\symfony\testDecembre> composer -v    
   ______
  / ____/___  ____ ___  ____  ____  ________  _____
 / /   / __ \/ __ `__ \/ __ \/ __ \/ ___/ _ \/ ___/
/ /___/ /_/ / / / / / / /_/ / /_/ (__  )  __/ /
\____/\____/_/ /_/ /_/ .___/\____/____/\___/_/
                    /_/
Composer version 2.4.2 2022-09-14 16:11:15

Usage:
  command [options] [arguments]

Options:
  -h, --help                     Display help for the given command. When no command is given display help for the list command
  -q, --quiet                    Do not output any message
  -V, --version                  Display this application version
      --ansi|--no-ansi           Force (or disable --no-ansi) ANSI output
  -n, --no-interaction           Do not ask any interactive question
      --profile                  Display timing and memory usage information
      --no-plugins               Whether to disable plugins.
      --no-scripts               Skips the execution of all scripts defined in composer.json file.
  -d, --working-dir=WORKING-DIR  If specified, use the given directory as working directory.
      --no-cache                 Prevent use of the cache
  -v|vv|vvv, --verbose           Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug

Available commands:
  about                Shows a short information about Composer
  archive              Creates an archive of this composer package
  audit                Checks for security vulnerability advisories for installed packages
  browse               [home] Opens the package's repository URL or homepage in your browser
  bump                 Increases the lower limit of your composer.json requirements to the currently installed versions
  check-platform-reqs  Check that platform requirements are satisfied
  clear-cache          [clearcache|cc] Clears composer's internal package cache
  completion           Dump the shell completion script
  config               Sets config options
  create-project       Creates new project from a package into given directory
  depends              [why] Shows which packages cause the given package to be installed
  diagnose             Diagnoses the system to identify common errors
  dump-autoload        [dumpautoload] Dumps the autoloader
  exec                 Executes a vendored binary/script
  fund                 Discover how to help fund the maintenance of your dependencies
  global               Allows running commands in the global composer dir ($COMPOSER_HOME)
  help                 Display help for a command
  init                 Creates a basic composer.json file in current directory
  install              [i] Installs the project dependencies from the composer.lock file if present, or falls back on the composer.json
  licenses             Shows information about licenses of dependencies
  list                 List commands
  outdated             Shows a list of installed packages that have updates available, including their latest version
  prohibits            [why-not] Shows which packages prevent the given package from being installed
  reinstall            Uninstalls and reinstalls the given package names
  remove               Removes a package from the require or require-dev
  require              [r] Adds required packages to your composer.json and installs them
  run-script           [run] Runs the scripts defined in composer.json
  search               Searches for packages
  self-update          [selfupdate] Updates composer.phar to the latest version
  show                 [info] Shows information about packages
  status               Shows a list of locally modified packages
  suggests             Shows package suggestions
  update               [u|upgrade] Updates your dependencies to the latest version according to composer.json, and updates the composer.lock file
  validate             Validates a composer.json and composer.lock
  ``` 
  ### 1.4 Version symfony  
  ```bash
  PS C:\wamp64\www\ADF22\adf-aurelia\symfony\testDecembre> symfony -v

 INFO  A new Symfony CLI version is available (5.4.19, currently running 5.4.16).

       If you installed the Symfony CLI via a package manager, updates are going to be automatic.
       If not, upgrade by downloading the new version at https://github.com/symfony-cli/symfony-cli/releases   
       And replace the current binary (symfony.exe) by the new one.

Symfony CLI version 5.4.16 (c) 2017-2022 Symfony SAS #StandWithUkraine Support Ukraine (2022-10-10T09:14:39Z - stable)
Symfony CLI helps developers manage projects, from local code to remote infrastructure

These are common commands used in various situations:

Work on a project locally

  new                                                            Create a new Symfony project
  server:start                                                   Run a local web server
  server:stop                                                    Stop the local web server
  security:check                                                 Check security issues in project dependencies 
  composer                                                       Runs Composer without memory limit
  console                                                        Runs the Symfony Console (bin/console) for current project
  php, pecl, pear, php-fpm, php-cgi, php-config, phpdbg, phpize  Runs the named binary using the configured PHP version

Manage a project on Cloud

  init                Initialize a new project using templates
  cloud:domains       Get a list of all domains
  cloud:branch        Branch an environment
  cloud:environments  Get a list of environments
  cloud:push          Push code to an environment
  cloud:ssh           SSH to the current environment
  cloud:projects      Get a list of all active projects
  cloud:tunnel:open   Open SSH tunnels to an app's relationships
  cloud:user:add      Add a user to the project
  cloud:variables     List variables

Show all commands with symfony.exe help,
Get help for a specific command with symfony.exe help COMMAND.
```

## 2. Installation de Symfony (regarder la doc)  
### 2.1. Installation
```bash
PS C:\wamp64\www\ADF22\adf-aurelia\symfony\testDecembre> symfony new --webapp my_project
```
ou à la racine attention le document doit être vide
```bash
PS C:\wamp64\www\ADF22\adf-aurelia\symfony\testDecembre> symfony new --webapp ./
 INFO  A new Symfony CLI version is available (5.4.19, currently running 5.4.16).

       If you installed the Symfony CLI via a package manager, updates are going to be automatic.
       If not, upgrade by downloading the new version at https://github.com/symfony-cli/symfony-cli/releases
       And replace the current binary (symfony.exe) by the new one.

* Creating a new Symfony project with Composer
  (running C:\ProgramData\ComposerSetup\bin\composer.phar create-project symfony/skeleton C:\wamp64\www\ADF22\adf-aurelia\symfony\testDecembre  --no-interaction)

* Setting up the project under Git version control
  (running git init C:\wamp64\www\ADF22\adf-aurelia\symfony\testDecembre)

  (running C:\ProgramData\ComposerSetup\bin\composer.phar require webapp --no-interaction)


 [OK] Your project is now ready in C:\wamp64\www\ADF22\adf-aurelia\symfony\testDecembre   
```
### 2.2. Configuration du .git
créer un projet blanc sur gitHub.
ajouter un nom le mettre en public et décocher la création du readme .
Suivre les instruction de gitHub:  
Git global setup
```bash
git config --global user.name "MESNIER Aurélia"
git config --global user.email "aurelia.mesnier@yahoo.fr"
```
Push an existing Git repository
```bash
cd existing_repo
git remote add origin https://gitlab.com/Aurelia1108/aurelia_8_dancing.git
git push -u origin --all
```

## 3. Création de la configuration locals (.env.local) : et création database  

copier et renommer .env en .env.local  

décommenter #DATABASE_URL, et recommenter l'autre, enregistrer.  

```
# DATABASE_URL="sqlite:///%kernel.project_dir%/var/data.db"
DATABASE_URL="mysql://app:!ChangeMe!@127.0.0.1:3306/app?serverVersion=8&charset=utf8mb4"
#DATABASE_URL="postgresql://app:!ChangeMe!@127.0.0.1:5432/app?serverVersion=14&charset=utf8"
```  
Modifier le login, le mot de passe et le nom de la base de données  
```
DATABASE_URL="mysql://app:!ChangeMe!@127.0.0.1:3306/app?serverVersion=8&charset=utf8mb4"

DATABASE_URL="mysql://root:@127.0.0.1:3306/ecommerce?serverVersion=8&charset=utf8mb4"
```

## 4. Démarrage server Symfony :  
attention une fois démarrer il ne faut pas fermer le terminal, donc pour les futurs lignes de codes il faut ouvrir un deuxième terminal.  

```bash
PS C:\wamp64\www\ADF22\adf-aurelia\symfony\testDecembre> symfony server:start

 INFO  A new Symfony CLI version is available (5.4.19, currently running 5.4.16).

       If you installed the Symfony CLI via a package manager, updates are going to be automatic.
       If not, upgrade by downloading the new version at https://github.com/symfony-cli/symfony-cli/releases   
       And replace the current binary (symfony.exe) by the new one.

                                                                                                               
 [WARNING] run "symfony.exe server:ca:install" first if you want to run the web server with TLS support, or u  
 se "--p12" or "--no-tls" to avoid this warning                                                                
                                                                                                               

Following Web Server log file (C:\Users\PC89\.symfony5\log\53df0560544905d5d521ded5a1355425b9bbed96.log)
Following PHP-CGI log file (C:\Users\PC89\.symfony5\log\53df0560544905d5d521ded5a1355425b9bbed96\79ca75f9e90b4126a5955a33ea6a41ec5e854698.log)

 [WARNING] The local web server is optimized for local development and MUST never be used in a production set  
 up.                                                                                                           
                                                                                                               


 [OK] Web server listening                                                                                     
      The Web server is using PHP CGI 8.1.0                                                                    
      http://127.0.0.1:8000                                                                                    
                                                                                                               

[Application] Dec 12 15:18:02 |INFO   | DEPREC User Deprecated: The "Monolog\Logger" class is considered final. It may change without further notice as of its next major version. You should not extend it from "Symfony\Bridge\Monolog\Logger".
[Web Server ] Dec 12 16:31:17 |DEBUG  | PHP    Reloading PHP versions
[Web Server ] Dec 12 16:31:18 |DEBUG  | PHP    Using PHP version 8.1.0 (from default version in $PATH)
[Web Server ] Dec 12 16:31:18 |INFO   | PHP    listening path="C:\\wamp64\\bin\\php\\php8.1.0\\php-cgi.exe" php="8.1.0" port=63902

```
On test l'accès à la première page  
Tester la 1er page en ouvrant http://127.0.0.1:8000   dans le navigateur  

## 5. Création du premier controller (page d'accueil /)  
dans un terminal différent du serveur symfony  
```bash
PS C:\wamp64\www\ADF22\adf-aurelia\symfony\testDecembre> php bin/console make:controller       

 Choose a name for your controller class (e.g. GrumpyGnomeController):
 >
```
```bash
 > Front

 created: src/Controller/FrontController.php
 created: templates/front/index.html.twig

 
  Success! 
 

 Next: Open your new controller class and add some pages!
 ```
 Pour ouvrir la page :  
 http://127.0.0.1:8000/front/

 Pour connaitre le nom dans l'URL il suffit d'aller dans le fichier src->Controller->FrontController  
 ```php
     #[Route('/front', name: 'app_front')]
 ```  
Pour transformer la page front en page d'acceuil (index) il suffit de vide le nom de la route une fois créee.  
```php
    #[Route('/', name: 'app_front')]
    public function index(): Response
    {
        return $this->render('front/index.html.twig', [
            'controller_name' => 'FrontController',
        ]);
    }
```

## 6. Création de nos entités à partir du MCD ou du diagramme de classe  
### 6.1 Création de la base de données  
```bash
PS C:\wamp64\www\ADF22\adf-aurelia\symfony\testDecembre> php ./bin/console doctrine:database:create
Created database `ecommerce` for connection named default
```

### 6.2 Création des entités sauf user
attention à la nommenclature
nom de l'entité en TopCamelCase
nom des propriétés en camelCase (on enlève les _)
pour les dates il faut mettre At à la fin sans _

```bash
PS C:\wamp64\www\ADF22\adf-aurelia\symfony\testDecembre> php ./bin/console make:entity             
 Class name of the entity to create or update (e.g. DeliciousChef):
 > Category

 created: src/Entity/Category.php
 created: src/Repository/CategoryRepository.php
 
 Entity generated! Now let's add some fields!
 You can always add more fields later manually or by re-running this command.

 New property name (press <return> to stop adding fields):
 > title

 Field type (enter ? to see all types) [string]:
 > string

 Field length [255]:
 >
text
 Can this field be null in the database (nullable) (yes/no) [no]:
 >

 updated: src/Entity/Category.php

 Add another property? Enter the property name (or press <return> to stop adding fields):      
 > slug

 Field type (enter ? to see all types) [string]:
 >

 Field length [255]:
 >

 Can this field be null in the database (nullable) (yes/no) [no]:
 >

 updated: src/Entity/Category.php

 Add another property? Enter the property name (or press <return> to stop adding fields):      
 >


 
  Success! 
 

 Next: When you're ready, create a migration with php bin/console make:migration

PS C:\wamp64\www\ADF22\adf-aurelia\symfony\testDecembre> php bin/console make:migration


 
  Success! 
 

 Next: Review the new migration "migrations/Version20221212160009.php"
 Then: Run the migration with php bin/console doctrine:migrations:migrate
 See https://symfony.com/doc/current/bundles/DoctrineMigrationsBundle/index.html
 ```
 Migration des entités créées:
 ```bash
 PS C:\wamp64\www\ADF22\adf-aurelia\symfony\testDecembre> php bin/console make:migration             

 [WARNING] You have 1 previously executed migrations in the database that are not registered   
           migrations.                                                                         
                                                                                               

 Are you sure you wish to continue? (yes/no) [yes]:
 >


 
  Success! 
 

 Next: Review the new migration "migrations/Version20221213093449.php"
 Then: Run the migration with php bin/console doctrine:migrations:migrate
 See https://symfony.com/doc/current/bundles/DoctrineMigrationsBundle/index.html  

PS C:\wamp64\www\ADF22\adf-aurelia\symfony\testDecembre> php bin/console doctrine:migrations:migrate

 WARNING! You are about to execute a migration in database "ecommerce" that could result in schema changes and data loss. Are you sure you wish to continue? (yes/no) [yes]:
 >


 [WARNING] You have 1 previously executed migrations in the database that are not registered  
           migrations.                                                                         
                                                                                               

 >> 2022-12-13 09:25:53 (DoctrineMigrations\Version20221212160009)

 Are you sure you wish to continue? (yes/no) [yes]:
 >

[notice] Migrating up to DoctrineMigrations\Version20221213093449
[notice] finished in 79.8ms, used 20M memory, 1 migrations executed, 2 sql queries

 ```

### 6.3 Création de l'entité user
Création de l'entité user 
````bash
PS C:\wamp64\www\ADF22\aurelia8dancing> php bin/console make:user

 The name of the security user class (e.g. User) [User]:
 > User

 Do you want to store user data in the database (via Doctrine)? (yes/no) [yes]:
 >

 Enter a property name that will be the unique "display" name for the user (e.g. email, username, uuid) [email]:
 >

 Will this app need to hash/check user passwords? Choose No if passwords are not needed or will be checked/hashed by some other system (e.g. a single sign-on server).

 Does this app need to hash/check user passwords? (yes/no) [yes]:
 >

 created: src/Entity/User.php
 created: src/Repository/UserRepository.php
 updated: src/Entity/User.php
 updated: config/packages/security.yaml

 
  Success! 
````

Ajouter les propriétés propres aux clients  
````bash
PS C:\wamp64\www\ADF22\aurelia8dancing> php ./bin/console make:entity User         

 Your entity already exists! So let's add some new fields!

 New property name (press <return> to stop adding fields):
 > firstname

 Field type (enter ? to see all types) [string]:
 > 

 Field length [255]:
 > 100

 Can this field be null in the database (nullable) (yes/no) [no]:
 >

 updated: src/Entity/User.php

 Add another property? Enter the property name (or press <return> to stop adding fields):
 > lastname

 Field type (enter ? to see all types) [string]:
 >

 Field length [255]:
 > 100

 Can this field be null in the database (nullable) (yes/no) [no]:
 >

 updated: src/Entity/User.php

 Add another property? Enter the property name (or press <return> to stop adding fields):
 > birthdayAt

 Field type (enter ? to see all types) [datetime_immutable]:
 >

 Can this field be null in the database (nullable) (yes/no) [no]:
 > yes

 updated: src/Entity/User.php

 Add another property? Enter the property name (or press <return> to stop adding fields):
 > adress1

 Field type (enter ? to see all types) [string]:
 >

 Field length [255]:
 >

 Can this field be null in the database (nullable) (yes/no) [no]:
 >

 updated: src/Entity/User.php

 Add another property? Enter the property name (or press <return> to stop adding fields):
 > adress2

 Field type (enter ? to see all types) [string]:
 >

 Field length [255]:
 >

 Can this field be null in the database (nullable) (yes/no) [no]:
 > yes

 updated: src/Entity/User.php

 Add another property? Enter the property name (or press <return> to stop adding fields):
 > zipCode

 Field type (enter ? to see all types) [string]:
 > integer

 Can this field be null in the database (nullable) (yes/no) [no]:
 >

 updated: src/Entity/User.php

 Add another property? Enter the property name (or press <return> to stop adding fields):
 > city

 Field type (enter ? to see all types) [string]:
 >

 Field length [255]:
 > 100

 Can this field be null in the database (nullable) (yes/no) [no]:
 >

 updated: src/Entity/User.php

 Add another property? Enter the property name (or press <return> to stop adding fields):
 > phone

 Field type (enter ? to see all types) [string]:
 >

 Field length [255]:
 > 100

 Can this field be null in the database (nullable) (yes/no) [no]:
 >

 updated: src/Entity/User.php

 Add another property? Enter the property name (or press <return> to stop adding fields):
 > mail

 Field type (enter ? to see all types) [string]:
 >

 Field length [255]:
 >

 Can this field be null in the database (nullable) (yes/no) [no]:
 >

 updated: src/Entity/User.php

 Add another property? Enter the property name (or press <return> to stop adding fields):
 > password



 [ERROR] The "password" property already exists.
                                                                                   

 Add another property? Enter the property name (or press <return> to stop adding fields):
 > slug

 Field type (enter ? to see all types) [string]:
 >

 Field length [255]:
 >

 Can this field be null in the database (nullable) (yes/no) [no]:
 >

 updated: src/Entity/User.php

 Add another property? Enter the property name (or press <return> to stop adding fields):
 > createdAt

 Field type (enter ? to see all types) [datetime_immutable]:
 >

 Can this field be null in the database (nullable) (yes/no) [no]:
 >

 updated: src/Entity/User.php

 Add another property? Enter the property name (or press <return> to stop adding fields):
 > modifiedAt

 Field type (enter ? to see all types) [datetime_immutable]:
 >

 Can this field be null in the database (nullable) (yes/no) [no]:
 >

 updated: src/Entity/User.php

 Add another property? Enter the property name (or press <return> to stop adding fields):
 >


 
  Success! 
 
````

## 7. Relation entre nos entités
pour créer une relation on appelle une classe et on lui ajoute une propriété du nom de la classe-relation

Relation manyToOne  

```bash
PS C:\wamp64\www\ADF22\adf-aurelia\symfony\testDecembre> php ./bin/console make:entity Product      
 Your entity already exists! So let's add some new fields!

 New property name (press <return> to stop adding fields):
 > category

 Field type (enter ? to see all types) [string]:
 > relation

 What class should this entity be related to?:
 > Category

What type of relationship is this?
 ------------ ------------------------------------------------------------------------ 
  Type         Description

 ------------ ------------------------------------------------------------------------
  ManyToOne    Each Product relates to (has) one Category.
               Each Category can relate to (can have) many Product objects.

  OneToMany    Each Product can relate to (can have) many Category objects.
               Each Category relates to (has) one Product.

  ManyToMany   Each Product can relate to (can have) many Category objects.
               Each Category can also relate to (can also have) many Product objects.

  OneToOne     Each Product relates to (has) exactly one Category.
               Each Category also relates to (has) exactly one Product.
 ------------ ------------------------------------------------------------------------

 Relation type? [ManyToOne, OneToMany, ManyToMany, OneToOne]:
 > ManyToOne

 Is the Product.category property allowed to be null (nullable)? (yes/no) [yes]:
 >

 Do you want to add a new property to Category so that you can access/update Product objects from it - e.g. $category->getProducts()? (yes/no) [yes]:
 > 

 A new property will also be added to the Category class so that you can access the related Product objects from it.

 New field name inside Category [products]:
 >

 Do you want to activate orphanRemoval on your relationship?
 A Products is "orphaned" when it is removed from its related Category.
 e.g. $category->removeProducts($products)

 NOTE: If a Products may *change* from one Category to another, answer "no".

 Do you want to automatically delete orphaned App\Entity\Products objects (orphanRemoval)? (yes/no) [no]:
 >
 updated: src/Entity/Product.php
 updated: src/Entity/Category.php

 Add another property? Enter the property name (or press <return> to stop adding fields):      
 >


 
  Success! 
 

 Next: When you're ready, create a migration with php bin/console make:migration

PS C:\wamp64\www\ADF22\adf-aurelia\symfony\testDecembre> php ./bin/console make:entity Product

 Your entity already exists! So let's add some new fields!

 New property name (press <return> to stop adding fields):
 > tva

 Field type (enter ? to see all types) [string]:
 > relation

 What class should this entity be related to?:
 > Tva

What type of relationship is this?
 ------------ -------------------------------------------------------------------
  Type         Description
 ------------ ------------------------------------------------------------------- 
  ManyToOne    Each Product relates to (has) one Tva.
               Each Tva can relate to (can have) many Product objects.

  OneToMany    Each Product can relate to (can have) many Tva objects.
               Each Tva relates to (has) one Product.

  ManyToMany   Each Product can relate to (can have) many Tva objects.
               Each Tva can also relate to (can also have) many Product objects.

  OneToOne     Each Product relates to (has) exactly one Tva.
               Each Tva also relates to (has) exactly one Product.
 ------------ -------------------------------------------------------------------

 Relation type? [ManyToOne, OneToMany, ManyToMany, OneToOne]:
 > ManyToOne

 Is the Product.tva property allowed to be null (nullable)? (yes/no) [yes]:
 >

 Do you want to add a new property to Tva so that you can access/update Product objects from it - e.g. $tva->getProducts()? (yes/no) [yes]:
 >

 A new property will also be added to the Tva class so that you can access the related Product objects from it.

 New field name inside Tva [products]:
 >

 updated: src/Entity/Product.php
 updated: src/Entity/Tva.php

 Add another property? Enter the property name (or press <return> to stop adding fields):      
 >


 
  Success! 
 

 Next: When you're ready, create a migration with php bin/console make:migration

PS C:\wamp64\www\ADF22\adf-aurelia\symfony\testDecembre> php bin/console make:migration             

 [WARNING] You have 1 previously executed migrations in the database that are not registered   
           migrations.                                                                         
                                                                                               

 Are you sure you wish to continue? (yes/no) [yes]:
 >          


 
  Success! 
 

 Next: Review the new migration "migrations/Version20221213102223.php"
 Then: Run the migration with php bin/console doctrine:migrations:migrate
 See https://symfony.com/doc/current/bundles/DoctrineMigrationsBundle/index.html
PS C:\wamp64\www\ADF22\adf-aurelia\symfony\testDecembre> php bin/console doctrine:migrations:migrate

 WARNING! You are about to execute a migration in database "ecommerce" that could result in schema changes and data loss. Are you sure you wish to continue? (yes/no) [yes]:
 >


 [WARNING] You have 1 previously executed migrations in the database that are not registered  
           migrations.                                                                         
                                                                                               

 >> 2022-12-13 09:25:53 (DoctrineMigrations\Version20221212160009)

 Are you sure you wish to continue? (yes/no) [yes]:
 >

[notice] Migrating up to DoctrineMigrations\Version20221213102223
[notice] finished in 1256.7ms, used 20M memory, 1 migrations executed, 5 sql queries
```
Autre exemple OneToMany  

````bash
PS C:\wamp64\www\ADF22\aurelia8dancing> php ./bin/console make:entity User   

 Your entity already exists! So let's add some new fields!

 New property name (press <return> to stop adding fields):
 > comment

 Field type (enter ? to see all types) [string]:
 > relation

 What class should this entity be related to?:
 > Comment

What type of relationship is this?
 ------------ -------------------------------------------------------------------- 
  Type         Description
 ------------ --------------------------------------------------------------------
  ManyToOne    Each User relates to (has) one Comment.
               Each Comment can relate to (can have) many User objects.

  OneToMany    Each User can relate to (can have) many Comment objects.
               Each Comment relates to (has) one User.

  ManyToMany   Each User can relate to (can have) many Comment objects.
               Each Comment can also relate to (can also have) many User objects.

  OneToOne     Each User relates to (has) exactly one Comment.
               Each Comment also relates to (has) exactly one User.
 ------------ --------------------------------------------------------------------

 Relation type? [ManyToOne, OneToMany, ManyToMany, OneToOne]:
 > OneToMany  

 A new property will also be added to the Comment class so that you can access and set the related User object from it.    

 New field name inside Comment [user]:
 >

 Is the Comment.user property allowed to be null (nullable)? (yes/no) [yes]:
 > no

 Do you want to activate orphanRemoval on your relationship?
 A Comment is "orphaned" when it is removed from its related User.
 e.g. $user->removeComment($comment)

 NOTE: If a Comment may *change* from one User to another, answer "no".

 Do you want to automatically delete orphaned App\Entity\Comment objects (orphanRemoval)? (yes/no) [no]:
 >

 updated: src/Entity/User.php
 updated: src/Entity/Comment.php
 ````


## 8. Migration vers la base de données ou Mise à jour du schéma

```bash
php bin/console make:migration  

php bin/console doctrine:migrations:migrate
```
## 9. Création de nos controllers admin (prefixer avec Admin : exemple : AdminProductController) pour créer de la donnée (CRUD). ¨Product - Category - Tva

CRUD Controllers permet de reunir les opérations CRUD (creer, afficher, ajouter, supprimer) pour une même classe et de créer une page et une route

Il faudra les faire au fur et à mesure des besoins.

```bash
PS C:\wamp64\www\ADF22\adf-aurelia\symfony\testDecembre> php bin/console make:crud      

 The class name of the entity to create CRUD (e.g. GentleChef):
 > Product

 Choose a name for your controller class (e.g. ProductController) [ProductController]:
 > AdminProductController

 Do you want to generate tests for the controller?. [Experimental] (yes/no) [no]:
 >

 created: src/Controller/AdminProductController.php
 created: src/Form/ProductType.php
 created: templates/admin_product/_delete_form.html.twig
 created: templates/admin_product/_form.html.twig
 created: templates/admin_product/edit.html.twig
 created: templates/admin_product/index.html.twig
 created: templates/admin_product/new.html.twig
 created: templates/admin_product/show.html.twig

 
  Success! 
 

 Next: Check your new CRUD by going to /admin/product/
 ```
 Pour ouvrir la page :  
 http://127.0.0.1:8000/admin/product/  

  Pour connaitre le nom dans l'URL il suffit d'aller dans le fichier src->Controller->AdminProductController  
 ```php
   #[Route('/admin/product')]
 ```  
Il suffit alors de recommencer l'opération pour chaque classe (entité).  

## 10. 

## 11. Eventuellement faire des Fixtures (création de jeux de données fake ou pas)  
suivre les étapes  

  ### 11.1. Installation de DoctrineFixtureBundle  
 ```bash
PS C:\wamp64\www\ADF22\adf-aurelia\symfony\testDecembre> composer require --dev orm-fixtures
```
  ### 11.2. Vérifier la configuration.  
Les fixtures sont uniquement dédiées à des environnements de dev, c’est pour cela qu’on précise que la configuration de ce bundle est uniquement liée à cet environnement. 
Dans le fichier **config->bundles**  
```php
    Doctrine\Bundle\FixturesBundle\DoctrineFixturesBundle::class => ['dev' => true, 'test' => true],

```
  ### 11.3. Installation de Faker  
```bash
PS C:\wamp64\www\ADF22\adf-aurelia\symfony\testDecembre> composer require fakerphp/faker
```

  ### 11.4. Création de données Fake  
    
Dans le fichier **src->DataFixture->AppFixtures** 

On modifie le commentaire

```php
<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }
}
```
On commence par ajouter faker
```php
        $faker = Factory::create('fr_FR');
```
Factory apparait comme erreur car la classe n'est pas appelée pour remédier à cela il suffit d'un clic droit sur la classe et Importer la classe.

```php
<?php

namespace App\DataFixtures;


use Faker\Factory;
use App\Entity\Tva;
use DateTimeImmutable;
use App\Entity\Product;
use App\Entity\Category;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\String\Slugger\SluggerInterface;

class AppFixtures extends Fixture
{
    protected $slugger;
        
    public function __construct(SluggerInterface $slugger)
    {
        $this->slugger = $slugger;
    }

    public function load(ObjectManager $manager): void
    {


        // $product = new Product();
        // $manager->persist($product);
        $faker = Factory::create('fr_FR');

        $tva=new Tva();
        $tva->setName('TVA20')
        ->setValue(20)
        ->setSlug('TVA20');
        $manager->persist($tva);

            for($i=0;$i<10;$i++){
                $category = new Category();
                $category->setTitle($faker->word())
                ->setSlug($this->slugger->slug($category->getTitle())->lower());

                $manager->persist($category);

                for($j=0;$j<10;$j++){
                    $product = new Product();
                    $product->setName($faker->word())
                    ->setSku($faker->ean13())
                    ->setDescription($faker->paragraph(2))
                    ->setPrice($faker->randomFloat(2))
                    ->setCreatedAt(DateTimeImmutable::createFromMutable($faker->datetime()))
                    ->setModifiedAt(DateTimeImmutable::createFromMutable($faker->datetime()))
                    ->setSlug($this->slugger->slug($product->getName())->lower())
                    ->setQuantity($faker->randomNumber(3, false))
                    ->setTva($tva);
                    
                    $manager->persist($product);
                }
              }
        $manager->flush();
    }
}
```

*Attention à bien compléter en fonction de sa propre base de données et des champs notifiés comme obligatoirement non nulle*


  ### 11.5. Injection dans la base de données  
```bash
PS C:\wamp64\www\ADF22\adf-aurelia\symfony\testDecembre> php bin/console doctrine:fixtures:load


 Careful, database "ecommerce" will be purged. Do you want to continue? (yes/no) [no]:
 > yes

   > purging database
   > loading App\DataFixtures\AppFixtures
```

## 12. Petites choses en plus :
  ### 12.1.  Modifier le Type (Form) : définir le type des champs nécessaire  

    Si l'on essaie de créer un nouveau produit, une erreur s'affiche car il est impossible dans l'état d'afficher la liste des catégories. Idem pour la tva.
    Pour y remédier il faut renseigner l'**entityType**.  

```php
public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('Name')
            ->add('sku')
            ->add('description')
            ->add('price')
            ->add('createdAt')
            ->add('modifiedAt')
            ->add('slug')
            ->add('quantity')
            ->add('category', EntityType::class, [
                'class'=>Category::class,
                'choice_label'=>'title',
            ])
            ->add('tva', EntityType::class,[
                'class'=>Tva::class,
                'choice_label'=>'name',
            ])
        ;
    }
```
Il faut penser à importer les classes appellées (EntityType, Category, Tva).  

Il faut aussi cacher les données de formulaire non nécéssaire comme le slug qui doit être automatisé, la date de création ou de modification... Mais avant de les suprimer du form il faut les gérer au niveau du submit dans le controller.  

  ### 12.2.  Gérer le slug  

Dans le controller, on genére le slug à partir du nom avant le submit.  
 ```php
    #[Route('/new', name: 'app_admin_product_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ProductRepository $productRepository, SluggerInterface $SluggerInterface): Response
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $product->setSlug($SluggerInterface->slug($product->getName())->lower());
            $productRepository->save($product, true);

            return $this->redirectToRoute('app_admin_product_index', [], Response::HTTP_SEE_OTHER);
        }
```

Dans le form on peut supprimer alors l'ajout du slug  

  ### 12.3.  Créer le layout de l'admin et l'associer aux vues admin  
base.html.twig que l'on copy et on modifie en fonction du nombre de vue front que l'on désire (admin.html.twig)

			base = vue client
			admin = vue dev

  ### 12.4.  Ajouter boostrap à l'admin et form twig avec bootstrap  

  Télécharger le css et le js de bootstrap et l'enregistrer dans le dossier Public->Asset.  (les fichiers .min) et créer dans le fichier **templates->base.html.twig** les balises pour le css et le js.  

  ```php
        {% block stylesheets %}
            <link href="{{ asset("asset/css/bootstrap.min.css") }}" rel="stylesheet">

            {{ encore_entry_link_tags('app') }}
        {% endblock %}

        {% block javascripts %}
           <script src="{{ asset("asset/js/bootstrap.min.js") }}"></script>

            {{ encore_entry_script_tags('app') }}
        {% endblock %}
  ```


  ### 12.5.  Ajouter une navigation à l'admin


  ### 12.6.  Optimiser l'ajout et l'édit dans les controller CRUD

  ### 12.7. Installation du webpack encore
  Sur le site *https://webpack.js.org/*, on trouve la documentation.
  Sur le site *https://symfony.com/doc/current/frontend.html#webpack-encore*, la documentation pour l'installation.

  #### 12.7.1. Installation du bundle  

```bash
PS C:\wamp64\www\ADF22\adf-aurelia\symfony\testDecembre> composer require symfony/webpack-encore-bundle
Info from https://repo.packagist.org: #StandWithUkraine
Using version ^1.16 for symfony/webpack-encore-bundle
./composer.json has been updated
Running composer update symfony/webpack-encore-bundle
Loading composer repositories with package information
Updating dependencies
Lock file operations: 1 install, 0 updates, 0 removals
  - Locking symfony/webpack-encore-bundle (v1.16.0)
Generating autoload files
110 packages you are using are looking for funding.
Use the `composer fund` command to find out more!

Symfony operations: 1 recipe (b65359ede5438d089c157ae7a363530b)
  - Configuring symfony/webpack-encore-bundle (>=1.10): From github.com/symfony/recipes:main
Executing script cache:clear [OK]
Executing script assets:install public [OK]

 What's next?
             

Some files have been created and/or updated to configure your new packages.
Please review, edit and commit them: these files are yours.

 symfony/webpack-encore-bundle  instructions:

  * Install Yarn and run yarn install

  * Start the development server: yarn encore dev-server

No security vulnerability advisories found
```

  #### 12.7.2. Installation de la dépendence npm

  *Remarque* : l'installation du yarn requière npm...

Tout d'abord télécharger node.js depuis *https://nodejs.org/en/download/* cliquer sur Windows Installer (node-v18.12.1.x64.msi). Et executer l'installation sur la machine (next sans rien cocher).

il faut fermer et redémarrer vs code attention à bien couper le serveur symfony (avec un ctrl+C dans le terminal du serveur symfony)

```bash
PS C:\wamp64\www\ADF22\adf-aurelia> npm -v     
8.19.2
PS C:\wamp64\www\ADF22\adf-aurelia\symfony\testDecembre> npm install
npm WARN deprecated stable@0.1.8: Modern JS already guarantees Array#sort() is a stable sort, so this library is deprecated. See the compatibility table on MDN: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/sort#browser_compatibility

added 630 packages, and audited 631 packages in 32s

63 packages are looking for funding
  run `npm fund` for details

found 0 vulnerabilities
npm notice
npm notice New major version of npm available! 8.19.2 -> 9.2.0
npm notice Changelog: https://github.com/npm/cli/releases/tag/v9.2.0
npm notice Run npm install -g npm@9.2.0 to update!
npm notice 
```

Puis on redemarre le serveur symfony.  

*Remarque : ces dépendances js peuvent être installer de manière glabale afin de servir à plusieurs projets.*

  #### 12.7.3. mettre à jour le dossier asset de public

Lancer un npm run dev (pour voir les modifications mais à faire après chaque modification) ou npm run build (optimisé afin d'être prêt pour la mise en ligne) ou npm run watch (en mode visible à chaque modification sans relire le webpack(si modification il faut refaire un run) comme avec SASS) afin de transpiler les asset à la racine pour les rendre visible dans le dossier public/asset.

```bash
PS C:\wamp64\www\ADF22\adf-aurelia\symfony\testDecembre> npm run dev

> dev
> encore dev

Running webpack ...

 DONE  Compiled successfully in 1569ms                                                 12:35:07

5 files written to public\build
Entrypoint app [big] 650 KiB = runtime.js 14.5 KiB vendors-node_modules_symfony_stimulus-bridge_dist_index_js-node_modules_core-js_modules_es_da-03e5c3.js 612 KiB app.css 741 bytes app.js 22.3 KiB
webpack compiled successfully
Installing shortcut: "C:\\Users\\PC89\\AppData\\Roaming\\Microsoft\\Windows\\Start Menu\\Programs\\SnoreToast\\0.7.0\\SnoreToast.lnk" "C:\\wamp64\\www\\ADF22\\adf-aurelia\\symfony\\testDecembre\\node_modules\\node-notifier\\vendor\\snoreToast\\snoretoast-x64.exe" Snore.DesktopToasts.0.7.0
```

  ###  12.8. Point d'entrée pour les différentes mise en forme

  Les différents points d'entrée sont défini dans le fichier

## 13. Upload des images
  ### 13.1. Modifier le fichier Form->CategoryType.php pour avoir un champ de type File  
```php
namespace App\Form;

use App\Entity\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title')
            ->add('picture', FileType::class,[
                'label' => 'Image',
                'mapped' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '10M',
                        'mimeTypes' => [
                            'image/jpeg',
                            'image/gif',
                            'image/png',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid image',
                    ])
                ],
            ])
        ;
    }

```
  ### 13.2. Modifier la sauvegarde de l'image  
On télécharge une image mais ce n'est pas une image qui est inclus dans la base de données mais le chemin de sa localisation. Dans le fichier **AdminCategoryController.php** :  
```php
    #[Route('/new', name: 'app_admin_category_new', methods: ['GET', 'POST'])]
    public function new(Request $request, CategoryRepository $categoryRepository, SluggerInterface $SluggerInterface): Response
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /**création du slug à partir du titre de la catégorie */
            $category->setSlug($SluggerInterface->slug($category->getTitle())->lower());
            /** @var UploadedFile $categoryFile */
            $categoryFile = $form->get('picture')->getData();

            // this condition is needed because the 'picture' field is not required
            // so the jgpeg, png or gif file must be processed only when a file is uploaded
            if ($categoryFile) {
                $originalFileName = pathinfo($categoryFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $SluggerInterface->slug($originalFileName);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$categoryFile->guessExtension();

                // Move the file to the directory where brochures are stored
                try {
                    $categoryFile->move(
                        $this->getParameter('uploads_directory').'pictureCategory/',
                        $newFilename
                    );
                } 
                catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }

                // updates the 'picture' property to store the PDF file name
                // instead of its contents
                $category->setPicture($newFilename);
            }
            $categoryRepository->save($category, true);

            return $this->redirectToRoute('app_admin_category_index', [], Response::HTTP_SEE_OTHER);
        }
```  
Pour faire apparaitre l'image il suffit de modifier le twig (**src->templates->admin_category->show.htlm.twig**):  

```twig
            <tr>
                <th>Slug</th>
                <td>{{ category.slug }}</td>
            </tr>
            <tr>
                <th>Picture</th>
                <td>
                    <img src="{{ asset('uploads/pictureCategory/'~category.picture ) }}" alt="si tu me vois ça marche pas!"/>
                </td>
            </tr>
```
{{asset()}} permet d'ouvrir le chemin dans public.

L'image n'apparait pas il faut donc modifier la configuration des services.
Dans le fichier **config->services.yaml**:  

```php
parameters:
    uploads_directory: '%kernel.project_dir%/public/uploads/'
```


### 13.3. Dans le controller tester la réception du fichier, testez l'upload directement à partir du controller  

Dans le fichier **AdminCategoryController**, on insert un dump and die (*dd()*) afin de voir le file uploadé.

```php
        if ($form->isSubmitted() && $form->isValid()) {
            /**création du slug à partir du titre de la catégorie */
            $category->setSlug($SluggerInterface->slug($category->getTitle())->lower());
            /** @var UploadedFile $categoryFile */
            $categoryFile = $form->get('picture')->getData();
                dd($categoryFile);
```
Cela va commencer le téléchargement et montrer les données du fichier téléchargées sans enregister le formulaire.  

```php
AdminCategoryController.php on line 38:
Symfony\Component\HttpFoundation\File\UploadedFile {#58 ▼
  -test: false
  -originalName: "couple_pixabay_Clker-Free-Vector-Images_1280.png"
  -mimeType: "image/png"
  -error: 0
  path: "C:\wamp64\tmp"
  filename: "phpBF8.tmp"
  basename: "phpBF8.tmp"
  pathname: "C:\wamp64\tmp\phpBF8.tmp"
  extension: "tmp"
  realPath: "C:\wamp64\tmp\phpBF8.tmp"
  aTime: 2022-12-15 09:57:35
  mTime: 2022-12-15 09:57:34
  cTime: 2022-12-15 09:57:34
  inode: 6473924464565690
  size: 119631
  perms: 0100666
  owner: 0
  group: 0
  type: "file"
  writable: true
  readable: true
  executable: false
  file: true
  dir: false
  link: false
  linkTarget: "C:\wamp64\tmp\phpBF8.tmp"
}
```


### 13.4. Créer un service d'unpload... le targetDirectory doir être public/uploads/... la méthode upload du service doit recevoir un deuxième paramètre $subDirectory. Par exemple 'category'. Le fichier sera donc uploadé dans public\uploads\category...

Créer une classe FileUploader en créant un dossier Service dans Src avec un fichier FileUploader.php contenant :

```php
<?php
namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\String\Slugger\SluggerInterface;

class FileUploader
{
    private $targetDirectory;
    private $slugger;

    public function __construct($targetDirectory, SluggerInterface $slugger)
    {
        $this->targetDirectory = $targetDirectory;
        $this->slugger = $slugger;
    }

    public function upload(UploadedFile $file)
    {
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = $this->slugger->slug($originalFilename);
        $fileName = $safeFilename.'-'.uniqid().'.'.$file->guessExtension();

        try {
            $file->move($this->getTargetDirectory(), $fileName);
        } catch (FileException $e) {
            // ... handle exception if something happens during file upload
        }

        return $fileName;
    }

    public function getTargetDirectory()
    {
        return $this->targetDirectory;
    }
}
```
Afin de définir le service rattaché à cette classe il faut modifier le fichier *services.yaml* dans le dossier **config**.  

```yaml
    # add more service definitions when explicit configuration is needed
    # please note that last definitions always *replace* previous ones

    # mise en service du service FileUploader 
    App\Service\FileUploader:
        arguments:
            $targetDirectory: '%uploads_directory%'
```

Il faut maintenant utiliser ce service dans le controller.  

```php
    #[Route('/new', name: 'app_admin_product_new', methods: ['GET', 'POST'])]
    public function new(Request $request, ProductRepository $productRepository, SluggerInterface $sluggerInterface,  FileUploader $fileUploader): Response
    {
        $product = new Product();
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $pictureFile */
            $pictureFile = $form->get('picture')->getData();
            // dd($pictureFile);
            if ($pictureFile) {
                $pictureFileName = $fileUploader->upload($pictureFile, 'pictureProduct');
                $product->setPicture($pictureFileName);
            }
            $product->setSlug($sluggerInterface->slug($product->getName())->lower());
            $productRepository->save($product, true);

            return $this->redirectToRoute('app_admin_product_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('admin_product/new.html.twig', [
            'product' => $product,
            'form' => $form,
        ]);
    }
```

Penser à importer les classes : FileUploader, UploadedFile.

Et à modifier le twig, pour faire apparaitre l'image.
(**src->templates->admin_category->show.htlm.twig**):  

```twig
            <tr>
                <th>Slug</th>
                <td>{{ category.slug }}</td>
            </tr>
            <tr>
                <th>Picture</th>
                <td>    
                    <img src="{{ asset('uploads/pictureProduct/'~product.picture ) }}" alt="si tu me vois ça marche pas!" width=150px/>
                </td>
            </tr>
```



## 14. Afficher sur le Front nos produits (sur la page front d'accueil)

## 15. Page de détails produits

## 16. Ajouter la gestion du multilangue (en|fr)

## 17. Ajouter une méthode Front pour la gestion des pages statiques (presentation mentions légales, contacts...)

## 18. Création du register client

````bash
PS C:\wamp64\www\ADF22\aurelia8dancing> php bin/console make:registration-form     

 Creating a registration form for App\Entity\User

 Do you want to add a @UniqueEntity validation annotation on your User class to make sure duplicate accounts aren't created? (yes/no) [yes]:
 >

 Do you want to send an email to verify the user's email address after registration? (yes/no) [yes]:
 > no

 Do you want to automatically authenticate the user after registration? (yes/no) [yes]:
 >

 updated: src/Entity/User.php
 created: src/Form/RegistrationFormType.php
 created: src/Controller/RegistrationController.php
 created: templates/registration/register.html.twig

 
  Success! 
 

 Next:
 Make any changes you need to the form, controller & template.

 Then open your browser, go to "/register" and enjoy your new form!
````

## 19. Création du login client

````bash
PS C:\wamp64\www\ADF22\aurelia8dancing> php bin/console make:auth                          
 What style of authentication do you want? [Empty authenticator]:
  [0] Empty authenticator
  [1] Login form authenticator
 > 1 

 The class name of the authenticator to create (e.g. AppCustomAuthenticator):        
 > LoginAuthenticator

 Choose a name for the controller class (e.g. SecurityController) [SecurityController]:
 > 

 Do you want to generate a '/logout' URL? (yes/no) [yes]:
 > 

 created: src/Security/LoginAuthenticator.php
 updated: config/packages/security.yaml
 created: src/Controller/SecurityController.php
 created: templates/security/login.html.twig

 
  Success! 
 

 Next:
 - Customize your new authenticator.
 - Finish the redirect "TODO" in the App\Security\LoginAuthenticator::onAuthenticationSuccess() method.
 - Review & adapt the login template: templates/security/login.html.twig.

````

## 20. Et après :
  ### 20.1.  Ajout au panier (session) 
  ### 20.2.  Validation de la commande
  ### 20.3.  Paiement !

***ATTENTION A LA NOMENCLATURE*** : 

- Nom controller : TopCamelCase exemple : AdminCategory ou  Front
- Nom entité : TopCamelCase au singulier : Exemple : Product, Category
- Propriété entités : camelCase... sans _ sansespace sans prefixe, sans accent.. .exemple : title, description  


*** Réinitialiser le cache et l'autoload***
```php
php bin/console cache:clear
```

```php
composer dump-autoload
```

