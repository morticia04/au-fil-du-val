<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230107171525 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE adresse ADD adresse_ligne1 VARCHAR(255) NOT NULL, ADD ville VARCHAR(255) NOT NULL, ADD pays VARCHAR(255) NOT NULL, ADD code_postal VARCHAR(255) NOT NULL, DROP adresse_line1, DROP zip_code, DROP city, DROP country, CHANGE adresse_line2 adresse_ligne2 VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE category DROP FOREIGN KEY FK_64C19C112469DE2');
        $this->addSql('ALTER TABLE category DROP FOREIGN KEY FK_64C19C14584665A');
        $this->addSql('DROP INDEX IDX_64C19C112469DE2 ON category');
        $this->addSql('DROP INDEX IDX_64C19C14584665A ON category');
        $this->addSql('ALTER TABLE category ADD titre VARCHAR(255) NOT NULL, ADD desciption VARCHAR(255) NOT NULL, ADD images VARCHAR(255) NOT NULL, DROP category_id, DROP product_id, DROP description, DROP title, DROP picture, DROP slug');
        $this->addSql('ALTER TABLE customer DROP FOREIGN KEY FK_81398E094DE7DC5C');
        $this->addSql('ALTER TABLE customer DROP FOREIGN KEY FK_81398E09A76ED395');
        $this->addSql('DROP INDEX IDX_81398E094DE7DC5C ON customer');
        $this->addSql('DROP INDEX IDX_81398E09A76ED395 ON customer');
        $this->addSql('ALTER TABLE customer ADD created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', DROP adresse_id, DROP user_id, CHANGE phone telephone VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE `order` DROP FOREIGN KEY FK_F529939864577843');
        $this->addSql('ALTER TABLE `order` DROP FOREIGN KEY FK_F52993989395C3F3');
        $this->addSql('DROP INDEX IDX_F529939864577843 ON `order`');
        $this->addSql('DROP INDEX IDX_F52993989395C3F3 ON `order`');
        $this->addSql('ALTER TABLE `order` ADD id_order VARCHAR(255) NOT NULL, DROP order_detail_id, DROP customer_id, DROP created_at, DROP shipping_cost');
        $this->addSql('ALTER TABLE order_detail ADD quantite VARCHAR(255) NOT NULL, ADD prix DOUBLE PRECISION NOT NULL, DROP quantity, DROP price');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD4D79775F');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD64577843');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04AD7D22E294');
        $this->addSql('DROP INDEX IDX_D34A04AD4D79775F ON product');
        $this->addSql('DROP INDEX IDX_D34A04AD7D22E294 ON product');
        $this->addSql('DROP INDEX IDX_D34A04AD64577843 ON product');
        $this->addSql('ALTER TABLE product ADD nom VARCHAR(255) NOT NULL, ADD quantite VARCHAR(255) NOT NULL, DROP tva_id, DROP product_picture_id, DROP order_detail_id, DROP name, DROP slug, CHANGE price prix DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE product_picture ADD titre VARCHAR(255) NOT NULL, ADD modified_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', DROP title, DROP file');
        $this->addSql('ALTER TABLE tva ADD nom VARCHAR(255) NOT NULL, ADD taux DOUBLE PRECISION NOT NULL, DROP name, DROP rate');
        $this->addSql('ALTER TABLE user ADD nom VARCHAR(255) NOT NULL, ADD prenom VARCHAR(255) NOT NULL, ADD pass_word VARCHAR(255) NOT NULL, ADD roles VARCHAR(255) NOT NULL, DROP role, DROP firstname, DROP lastname, DROP password');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE customer ADD adresse_id INT DEFAULT NULL, ADD user_id INT DEFAULT NULL, DROP created_at, CHANGE telephone phone VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE customer ADD CONSTRAINT FK_81398E094DE7DC5C FOREIGN KEY (adresse_id) REFERENCES adresse (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE customer ADD CONSTRAINT FK_81398E09A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_81398E094DE7DC5C ON customer (adresse_id)');
        $this->addSql('CREATE INDEX IDX_81398E09A76ED395 ON customer (user_id)');
        $this->addSql('ALTER TABLE product_picture ADD file VARCHAR(255) NOT NULL, DROP modified_at, CHANGE titre title VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE product ADD tva_id INT DEFAULT NULL, ADD product_picture_id INT DEFAULT NULL, ADD order_detail_id INT DEFAULT NULL, ADD name VARCHAR(255) NOT NULL, ADD slug VARCHAR(255) NOT NULL, DROP nom, DROP quantite, CHANGE prix price DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD4D79775F FOREIGN KEY (tva_id) REFERENCES tva (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD64577843 FOREIGN KEY (order_detail_id) REFERENCES order_detail (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04AD7D22E294 FOREIGN KEY (product_picture_id) REFERENCES product_picture (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_D34A04AD4D79775F ON product (tva_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD7D22E294 ON product (product_picture_id)');
        $this->addSql('CREATE INDEX IDX_D34A04AD64577843 ON product (order_detail_id)');
        $this->addSql('ALTER TABLE `order` ADD order_detail_id INT DEFAULT NULL, ADD customer_id INT DEFAULT NULL, ADD created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', ADD shipping_cost DOUBLE PRECISION NOT NULL, DROP id_order');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F529939864577843 FOREIGN KEY (order_detail_id) REFERENCES order_detail (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE `order` ADD CONSTRAINT FK_F52993989395C3F3 FOREIGN KEY (customer_id) REFERENCES customer (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_F529939864577843 ON `order` (order_detail_id)');
        $this->addSql('CREATE INDEX IDX_F52993989395C3F3 ON `order` (customer_id)');
        $this->addSql('ALTER TABLE adresse ADD adresse_line1 VARCHAR(255) NOT NULL, ADD zip_code VARCHAR(255) NOT NULL, ADD city VARCHAR(255) NOT NULL, ADD country VARCHAR(255) NOT NULL, DROP adresse_ligne1, DROP ville, DROP pays, DROP code_postal, CHANGE adresse_ligne2 adresse_line2 VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE order_detail ADD price VARCHAR(255) NOT NULL, DROP prix, CHANGE quantite quantity VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE user ADD role VARCHAR(255) NOT NULL, ADD firstname VARCHAR(255) NOT NULL, ADD lastname VARCHAR(255) NOT NULL, ADD password VARCHAR(255) NOT NULL, DROP nom, DROP prenom, DROP pass_word, DROP roles');
        $this->addSql('ALTER TABLE tva ADD rate VARCHAR(255) NOT NULL, DROP taux, CHANGE nom name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE category ADD category_id INT DEFAULT NULL, ADD product_id INT DEFAULT NULL, ADD description VARCHAR(255) NOT NULL, ADD title VARCHAR(255) NOT NULL, ADD picture VARCHAR(255) NOT NULL, ADD slug VARCHAR(255) NOT NULL, DROP titre, DROP desciption, DROP images');
        $this->addSql('ALTER TABLE category ADD CONSTRAINT FK_64C19C112469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE category ADD CONSTRAINT FK_64C19C14584665A FOREIGN KEY (product_id) REFERENCES product (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_64C19C112469DE2 ON category (category_id)');
        $this->addSql('CREATE INDEX IDX_64C19C14584665A ON category (product_id)');
    }
}
