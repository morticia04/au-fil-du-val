<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230112160840 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE adresse DROP id_adresse');
        $this->addSql('ALTER TABLE category CHANGE images images VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE customer DROP id_customer');
        $this->addSql('ALTER TABLE `order` DROP id_order');
        $this->addSql('ALTER TABLE order_detail DROP id_order_detail');
        $this->addSql('ALTER TABLE product_picture DROP id_product_picture');
        $this->addSql('ALTER TABLE user CHANGE create_at create_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\'');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE customer ADD id_customer VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE category CHANGE images images VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE product_picture ADD id_product_picture VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE adresse ADD id_adresse VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE order_detail ADD id_order_detail VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE user CHANGE create_at create_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE `order` ADD id_order VARCHAR(255) NOT NULL');
    }
}
