<?php

namespace App\Controller;

use App\Entity\Category;
use App\Form\CategoryType;
use App\Service\FileUploader;
use App\Repository\CategoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;

#[Route('/admin/category')]
class AdminCategoryController extends AbstractController
{
    #[Route('/', name: 'app_admin_category_index', methods: ['GET'])]
    public function index(CategoryRepository $categoryRepository): Response
    {
        return $this->render('admin_category/index.html.twig', [
            'categories' => $categoryRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_admin_category_new', methods: ['GET', 'POST'])]
    #[Route('/{id}/edit', name: 'app_admin_category_edit', methods: ['GET', 'POST'])]
    public function new(Category $category=null, Request $request, CategoryRepository $categoryRepository, FileUploader  $fileUploader, EntityManagerInterface $entityManager, SluggerInterface $SluggerInterface): Response
    {
        $category = new Category();
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            /**création du slug à partir du titre de la catégorie */
            // $category->setSlug($SluggerInterface->slug($category->getTitle())->lower());
            /** @var UploadedFile $categoryFile */
            $categoryFile = $form->get('images')->getData();
            // dd($categoryFile);
            // this condition is needed because the 'picture' field is not required
            // so the jgpeg, png or gif file must be processed only when a file is uploaded



            if ($categoryFile) {
                $originalFileName = pathinfo($categoryFile->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the file name as part of the URL
                $safeFilename = $SluggerInterface->slug($originalFileName);
                $newFilename = $safeFilename . '-' . uniqid() . '.' . $categoryFile->guessExtension();
                $path = $fileUploader->upload($categoryFile, 'category');
                $category->setImages($path);

                // Move the file to the directory where brochures are stored
                try {
                    $categoryFile->move(
                        $this->getParameter('uploads_directory') . 'imagesCategory/',
                        $newFilename
                    );
                } catch (FileException $e) {
                    // ... handle exception if something happens during file upload
                }


                $entityManager->persist($category);
                $entityManager->flush();


                // updates the 'picture' property to store the PDF file name
                // instead of its contents
                $category->setImages($newFilename);
            }

            $categoryRepository->save($category, true);
            return $this->redirectToRoute('app_admin_category_index', [], Response::HTTP_SEE_OTHER);


            return $this->render('admin_category/new.html.twig', [
                'edit' => $category->getId(),
                'category' => $category,
                'form' => $form,
            ]);
        }
    }

    #[Route('/{id}', name: 'app_admin_category_show', methods: ['GET'])]
    public function show(Category $category): Response
    {       

        return $this->render('admin_category/show.html.twig', [
            'category' => $category,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_admin_category_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Category $category, CategoryRepository $categoryRepository, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(CategoryType::class, $category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $categoryRepository->save($category, true);

            return $this->redirectToRoute('app_admin_category_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('admin_category/edit.html.twig', [
            'category' => $category,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_category_delete', methods: ['POST'])]
    public function delete(Request $request, Category $category, CategoryRepository $categoryRepository, EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete' . $category->getId(), $request->request->get('_token'))) {
            $categoryRepository->remove($category, true);

            $entityManager->remove($category);
            $entityManager->flush();
        }

        return $this->redirectToRoute('app_admin_category_index', [], Response::HTTP_SEE_OTHER);
    }
}
