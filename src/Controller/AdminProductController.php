<?php

namespace App\Controller;

use App\Entity\Product;
use App\Form\ProductType;
use App\Service\Uploader;
use App\Service\FileUploader;
use App\Entity\ProductPicture;
use App\Form\ProductPictureType;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\ProductPictureRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/admin/product')]
class AdminProductController extends AbstractController
{
    #[Route('/', name: 'app_admin_product_index', methods: ['GET'])]
    public function index(ProductRepository $productRepository): Response
    {
        return $this->render('admin_product/index.html.twig', [
            'products' => $productRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_admin_product_new', methods: ['GET', 'POST'])]   
        public function new(Request $request, Product $product=null, ProductRepository $productRepository, ProductPictureRepository $productPictureRepository, FileUploader $uploader): Response
    {   
        if($product==null)
            $product = new Product();

        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            
            $productRepository->save($product, true);

            return $this->redirectToRoute('app_admin_product_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('admin_product/new.html.twig', [
            'product' => $product,            
            'form' => $form,
        ]);
    }
   

    #[Route('/{id}/edit', name: 'app_admin_product_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Product $product, ProductRepository $productRepository, FileUploader $uploader): Response
    {
        $form = $this->createForm(ProductType::class, $product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $productRepository->save($product, true);

            return $this->redirectToRoute('app_admin_product_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('admin_product/edit.html.twig', [
            'product' => $product,
            'form' => $form,
        ]);
    }

    #[Route('/{id}/pictures', name: 'app_admin_product_pictures', methods: ['GET'])]
    public function listPictures(Product $product): Response
    {
        dump($product);
        return $this->render('admin_product/listPictures.html.twig', [
            'product' => $product
        ]);
    }

    #[Route('/{id}/pictures/new', name: 'app_admin_product_pictures_new', methods: ['GET', 'POST'])]
    public function newPicture(Product $product, Request $request,  ProductPictureRepository $pictureRepository, FileUploader $uploader): Response
    {
        $picture = new ProductPicture();
        $form = $this->createForm(ProductPictureType::class, $picture);
        $form->handleRequest($request);

        

        if ($form->isSubmitted() && $form->isValid()) {            
            $picture->setProduct($product);
            $pictureUpload = $form->get('picture')->getData();

            if ($pictureUpload) {
                $path = $uploader->upload($pictureUpload, 'product');
                $picture->setPath($path);
            }
            $pictureRepository->add($picture, true);

            return $this->redirectToRoute('app_admin_product_pictures', ['id'=> $product->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('admin_product/newPicture.html.twig', [
            'form' => $form,
            'product' => $product,
            'edit' => $picture->getId()
        ]);
    }

    #[Route('/{id}/pictures/edit', name: 'app_admin_product_pictures_edit', methods: ['GET', 'POST'])]
    public function pictureEdit(Product $product, Request $request,  ProductPictureRepository $pictureRepository, FileUploader $uploader): Response
    {
        $picture = new ProductPicture();
        $form = $this->createForm(ProductPictureType::class, $picture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {            
            $picture->setProduct($product);
            $pictureUpload = $form->get('picture')->getData();

            if ($pictureUpload) {
                $path = $uploader->upload($pictureUpload, 'product');
                $picture->setPath($path);
            }
            $pictureRepository->add($picture, true);

            return $this->redirectToRoute('app_admin_product_pictures_edit', ['id'=> $product->getId()], Response::HTTP_SEE_OTHER);
        }

        return $this->render('admin_product/newPicture.html.twig', [
            'form' => $form,
            'product' => $product,
            'edit' => $picture->getId()
        ]);
    }

    #[Route('/{id}', name: 'app_admin_product_show', methods: ['GET'])]
    public function show(Product $product): Response
    {
        return $this->render('admin_product/show.html.twig', [
            'product' => $product,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_product_delete', methods: ['POST'])]
    public function delete(Request $request, Product $product, ProductRepository $productRepository,EntityManagerInterface $entityManager): Response
    {
        if ($this->isCsrfTokenValid('delete'.$product->getId(), $request->request->get('_token'))) {
            $productRepository->remove($product, true);
        }

        return $this->redirectToRoute('app_admin_product_index', [], Response::HTTP_SEE_OTHER);
    }
}
