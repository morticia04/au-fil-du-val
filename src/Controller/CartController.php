<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CartController extends AbstractController
{
    #[Route('/panier', name: 'panier')]   
    public function index(): Response
    {
        return $this->render('front/panier.html.twig', [
            'controller_name' => 'CartController',
        ]);
    }

    #[Route('/panier/add/{id}', name: 'panier_add')]
    public function add($id, Request $request): Response
    {   
        $session = $request->getSession();
        $panier = $session->get('panier',[]);
        $panier[$id]=1;
        $session->set('panier', $panier);
       

        return $this->render('front/panier.html.twig', [
            'controller_name' => 'CartController',
        ]);
    }

}
