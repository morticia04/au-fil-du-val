<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FrontController extends AbstractController

/**Page d'accueil du Site on enlève front aprés "/" et renomer "Home" */
{
    #[Route('/', name: 'home')]
    public function index(): Response
    {
        return $this->render('front/index.html.twig', [
            'title' => "A Propos"
        ]);
    }

    /**Page catalogue */
    #[Route('/catalogue', name: 'catalogue')]
    public function catalogue(): Response
    {
        return $this->render('front/catalogue.html.twig', [
            'title' => "Catalogue"
        ]);
    }

    /**Page bandeau */
    #[Route('/bandeau', name: 'bandeau')]
    public function bandeau(): Response
    {
        return $this->render('front/bandeau.html.twig', [
            'title' => "Bandeau"
        ]);
    }

    /**Page bonnets */
    #[Route('/bonnet', name: 'bonnets')]
    public function bonnets(): Response
    {
        return $this->render('front/bonnet.html.twig', [
            'title' => "Bonnets"
        ]);
    }

    /**Page contact */
    #[Route('/contact', name: 'contact')]
    public function contact(): Response
    {
        return $this->render('front/contact.html.twig',);
    }

    
    
    
    /**Page menage */
    #[Route('/menage', name: 'menage')]
    public function menage(): Response
    {
        return $this->render('front/menage.html.twig', [
            'title' => "Ménage"
        ]);
    }

    #[Route('/mode', name: 'mode')]
    public function mode(): Response
    {
        return $this->render('front/mode.html.twig', [
            'title' => "Mode"
        ]);
    }

    /**Page panier */
    #[Route('/panier', name: 'panier')]
    public function panier(): Response
    {
        return $this->render('front/panier.html.twig',);
    }

    /**Page pelote */
    #[Route('/pelote', name: 'pelote')]
    public function pelote(): Response
    {
        return $this->render('front/pelote.html.twig', [
            'title' => "Pelotes"
        ]);
    }

    /**Page Salle de Bain */
    #[Route('/sdb', name: 'sdb')]
    public function sdb(): Response
    {
        return $this->render('front/sdb.html.twig', [
            'title' => "Salle de Bain"
        ]);
    }

    /**Page Snoods */
    #[Route('/snoods', name: 'snoods')]
    public function snoods(): Response
    {
        return $this->render('front/snoods.html.twig', [
            'title' => "Snoods et Echarpes"
        ]);
    }
}
