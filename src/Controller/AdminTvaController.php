<?php

namespace App\Controller;

use App\Entity\Tva;
use App\Form\TvaType;
use App\Repository\TvaRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

#[Route('/admin/tva')]
class AdminTvaController extends AbstractController
{
    #[Route('/', name: 'app_admin_tva_index', methods: ['GET'])]
    public function index(TvaRepository $tvaRepository): Response
    {
        return $this->render('admin_tva/index.html.twig', [
            'tvas' => $tvaRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_admin_tva_new', methods: ['GET', 'POST'])]
    public function new(Request $request, TvaRepository $tvaRepository, EntityManagerInterface $entityManager): Response
    {
        $tva = new Tva();
        $form = $this->createForm(TvaType::class, $tva);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tvaRepository->save($tva, true);
            $entityManager->persist($tva);
            $entityManager->flush();


            return $this->redirectToRoute('app_admin_tva_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('admin_tva/new.html.twig', [
            'tva' => $tva,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_tva_show', methods: ['GET'])]
    public function show(Tva $tva): Response
    {
        return $this->render('admin_tva/show.html.twig', [
            'tva' => $tva,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_admin_tva_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, Tva $tva, TvaRepository $tvaRepository, EntityManagerInterface $entityManager): Response
    {
        $form = $this->createForm(TvaType::class, $tva);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tvaRepository->save($tva, true);
            $entityManager->flush();

            return $this->redirectToRoute('app_admin_tva_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('admin_tva/edit.html.twig', [
            'tva' => $tva,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_admin_tva_delete', methods: ['POST'])]
    public function delete(Request $request, Tva $tva, TvaRepository $tvaRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$tva->getId(), $request->request->get('_token'))) {
            $tvaRepository->remove($tva, true);
        }

        return $this->redirectToRoute('app_admin_tva_index', [], Response::HTTP_SEE_OTHER);
    }
}
