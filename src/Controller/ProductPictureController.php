<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductPictureController extends AbstractController
{
    #[Route('/product/pictures', name: 'app_product_pictures')]
    public function index(): Response
    {
        return $this->render('product_pictures/index.html.twig', [
            'controller_name' => 'ProductPictureController',
        ]);
    }
}
